# Interview-Questrade-GCP

Problem statement
You are tasked with deploying a secure infrastructure on Google Cloud Platform (GCP) using Terraform and automating the deployment process with GitLab CI. Your infrastructure should consist of the following components:


- Virtual Private Cloud (VPC) with private and public subnets.

- Two Compute Engine instances: one in the public subnet and one in the private subnet.
 
- Nginx installed and running on the Compute Engine instance in the public subnet, serving a static HTML page.
 
- The Compute Engine instance in the private subnet should not have direct internet access.

Requirements:

- Use Terraform to define and provision the infrastructure resources on GCP.

- Create a GitLab CI pipeline that triggers the infrastructure deployment on every commit to the main branch.

- Ensure that the pipeline fails if there are any syntax or linting errors in the Terraform configuration.
 
- Store sensitive information (e.g., GCP service account credentials) securely, following best practices.
 
- Use GitLab CI variables to store and retrieve sensitive information within the pipeline.


**Implement appropriate security measures, such as firewall rules and VPC service controls, to restrict access and enhance the security posture of the infrastructure.**


Brief information about the pipeline:

- The CI/CD pipeline will be triggered/executed automatically, if a commit happens in the main branch.
- The pipeline consists of 4 stages, validate, plan, deploy, cleanup.
- Each stage has one job each, which triggers on various conditions like successfull execution of a previous job or if there's a change in any specific file, etc.
- The validate stage validates the syntax of all the terraform files and initiates terraform with no backend (for the sake of simplicity)
- In the plan stage, terraform plans on what resources to build and in what order based on the configuration and current state.
- In the deploy stage, terraform applies the configuration planned in the previous state and this stage will only run if the plan stage is executed successfully.
- The deploy stage also produces some artifacts like terraform.tfstate and terraform.tfstate.backup that'd be used by the next stage.
- In the destroy stage, the container sleeps for 30 seconds so that the user could show the infrastructure built by terraform and destroys it based on the terraform.tfstate file present as an artifact.

