# main.tf

# Configure the Google Cloud provider 
provider "google" {
  project     = var.PROJECT_ID
  credentials = var.SERVICE_ACCOUNT
  region      = var.REGION
}

# Create a VPC network
resource "google_compute_network" "vpc_network" {
  name = "questrade-vpc-network"
  auto_create_subnetworks = false
}

# Create a public subnet
resource "google_compute_subnetwork" "public_subnet" {
  name          = "public-subnet"
  ip_cidr_range = "10.0.1.0/24"
  network       = google_compute_network.vpc_network.self_link
}

# Create a private subnet
resource "google_compute_subnetwork" "private_subnet" {
  name          = "private-subnet"
  ip_cidr_range = "10.0.2.0/24"
  network       = google_compute_network.vpc_network.self_link
}

# Create a firewall rule to allow HTTP traffic
resource "google_compute_firewall" "http_firewall" {
  name    = "allow-http"
  network = google_compute_network.vpc_network.name

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["http"]
}

# Create a Compute Engine instance in the public subnet
resource "google_compute_instance" "public_instance" {
  name         = "public-instance"
  machine_type = "e2-micro"
  zone         = var.ZONE

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network       = google_compute_network.vpc_network.self_link
    subnetwork    = google_compute_subnetwork.public_subnet.self_link
    access_config {
      nat_ip = google_compute_address.public_ip.address
    }
  }

  metadata_startup_script = <<-EOF
    #!/bin/bash
    apt-get update
    apt-get install -y nginx
    echo 'Hello Questrade' | tee /var/www/html/index.html
    service nginx start
  EOF

  tags = ["http"]
}

# Create a Compute Engine instance in the private subnet
resource "google_compute_instance" "private_instance" {
  name         = "private-instance"
  machine_type = "e2-micro"
  zone         = var.ZONE

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network    = google_compute_network.vpc_network.self_link
    subnetwork = google_compute_subnetwork.private_subnet.self_link
  }
}

# Create a static IP address for the public instance
resource "google_compute_address" "public_ip" {
  name = "public-ip"
  region = var.REGION
}
