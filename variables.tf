# variables.tf

variable "PROJECT_ID" {
  default = ""
}

variable "SERVICE_ACCOUNT" {
  default = ""
}

variable "REGION" {
  default = ""
}

variable "ZONE" {
  default = ""
}
